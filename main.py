import cv2
import os
# import datetime
import time

from src.detect.detect_utils import DetectUtils
from src.show.show_utils import ShowUtils
from src.track.track_utils import TrackUtils
# from utils.common import logger


from utils.settings import TRACKER, DETECTOR, B_SAVE_VIDEO, B_SHOW_VIDEO
from utils.constant import SKIP1, SKIP2, SCALE_FACTOR


try:
    from utils.local_settings import *
except Exception as e:
    print(e)


class RetailAnalysis:
    def __init__(self):

        self.cap = None

        self.detector = DetectUtils(det_mode=DETECTOR).detector
        self.tracker = TrackUtils(trk_type=TRACKER)
        self.shower = ShowUtils(b_log=False)

        self.fourcc = cv2.VideoWriter_fourcc(*'XVID')
        self.saver = None

        self.scale_factor = SCALE_FACTOR

    def run(self, video_path):
        # ---------------- initialize -------------------------------------------------
        self.cap = cv2.VideoCapture(video_path)

        fps = self.cap.get(cv2.CAP_PROP_FPS)
        width = self.cap.get(cv2.CAP_PROP_FRAME_WIDTH) * self.scale_factor
        height = self.cap.get(cv2.CAP_PROP_FRAME_HEIGHT) * self.scale_factor
        num_frames = self.cap.get(cv2.CAP_PROP_FRAME_COUNT)
        _log_msg = ("video infos:\n" +
                    "\tfps: {}\n".format(fps) +
                    "\twidth: {}\n".format(width) +
                    "\theight: {}\n".format(height) +
                    "\tnum_frames: {}\n".format(num_frames))
        print(_log_msg)

        if B_SAVE_VIDEO:
            output_path = os.path.join(os.path.dirname(video_path), "output.avi")
            self.saver = cv2.VideoWriter(output_path, self.fourcc, fps, (int(width), int(height)))
        else:
            self.saver = None

        print("start analyzing...")
        interval = fps * 10
        frm_cnt = -1
        persons = {}

        __last_fps_t = 0

        while True:
            frm_cnt += 1
            ret, raw_frame = self.cap.read()

            if not ret:
                break

            if frm_cnt % int(interval) == 0:
                print("{} / {}".format(frm_cnt, num_frames))
                __dur = time.time() - __last_fps_t
                __fps = interval / __dur
                print("fps: {}".format(round(__fps, 2)))
                __last_fps_t = time.time()

            # skip1
            if frm_cnt % SKIP1 != 0:
                continue

            # -------------------- pre-processing (undistorting) -------------------------------------------------------
            frame = cv2.resize(raw_frame, None, fx=self.scale_factor, fy=self.scale_factor)

            # -------------------- detect and tracking -----------------------------------------------------------------
            if frm_cnt % SKIP2 == 0:  # detect the object

                # detect the object ----------------------------------------------------
                dets = self.detector.detect(img=frame)
                # show_img = self.shower.show_objects(img=frame, objects=dets)

                # update trackers ------------------------------------------------------
                self.tracker.upgrade_trackers(dets=dets, trk_img=frame, trackers=persons)

            else:
                # keep trackers --------------------------------------------------------
                self.tracker.keep_trackers(trk_img=frame, trackers=persons)

            # -------------------- show the frame ----------------------------------------------------------------------
            show_img = self.shower.show_trackers(trk_img=frame, trackers=persons)

            if B_SHOW_VIDEO:
                cv2.imshow("result", show_img)
                key = cv2.waitKey(1)
                if key == ord('q'):
                    break
                elif key == ord('n'):  # next
                    pos = self.cap.get(cv2.CAP_PROP_POS_FRAMES)
                    pos += 100
                    frm_cnt += 100
                    self.cap.set(cv2.CAP_PROP_POS_FRAMES, pos)
                    print(frm_cnt)
                elif key == ord('p'):  # prev
                    pos = self.cap.get(cv2.CAP_PROP_POS_FRAMES)
                    pos -= max(0, pos - 100)
                    frm_cnt -= 100
                    self.cap.set(cv2.CAP_PROP_POS_FRAMES, pos)
                    print(frm_cnt)

            if B_SAVE_VIDEO:
                self.saver.write(show_img)

        self.release()
        if not B_SAVE_VIDEO:  # rename
            os.remove(video_path)
            time.sleep(1)

    def release(self):
        if self.saver is not None:
            self.saver.release()
        if self.cap is not None:
            self.cap.release()


if __name__ == '__main__':
    ra = RetailAnalysis()

    path = "data/videos/heatmap-2.mov"
    ra.run(video_path=path)
