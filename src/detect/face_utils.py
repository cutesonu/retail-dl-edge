import os
import dlib
import numpy as np

from utils.constant import MODEL_DIR, KEY_LABEL, KEY_CONFIDENCE


class FaceUtils:
    def __init__(self, mode="HOG"):
        hog_face_detector = dlib.get_frontal_face_detector()

        # initialize cnn based face detector with the weights
        weights_path = os.path.join(MODEL_DIR, "dlib", "mmod_human_face_detector.dat")
        cnn_face_detector = dlib.cnn_face_detection_model_v1(weights_path)

        if mode == "HOG":
            self.detector = hog_face_detector
        else:
            self.detector = cnn_face_detector

    def detect(self, img):
        im_h, im_w = img.shape[:2]
        faces = self.detector(img, 1)

        objs = []

        for face in faces:
            x = face.left()
            y = face.top()
            w = face.right() - x
            h = face.bottom() - y

            objs.append({
                KEY_LABEL: "face",
                KEY_BOX: (np.divide(np.array([x, y, x + w, y + h]), np.array([im_w, im_h, im_w, im_h]))).astype(
                    np.float),
                KEY_CONFIDENCE: 100.0
            })
        return objs
