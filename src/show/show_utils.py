import cv2
import numpy as np
from utils.constant import KEY_FRECT, KEY_LABEL, KEY_RECT, KEY_CONFIDENCE, KEY_COLOR


# import utils.logger as logger


COLOR_RECT_FILL = (255, 125, 0)
COLOR_RECT_BORDER = (0, 0, 255)
COLOR_TEXT = (0, 0, 255)


class ShowUtils:
    def __init__(self, b_log=True):
        self.b_log = b_log

    @staticmethod
    def show_objects(img, objects, offset=(0, 0)):
        ofst_x, ofst_y = offset
        show_img = img.copy()
        img_h, img_w = img.shape[:2]

        for obj in objects:
            (x, y, x2, y2) = (obj[KEY_FRECT] * np.array([img_w, img_h, img_w, img_h])).astype(np.int)

            label = obj[KEY_LABEL]
            confidence = float(obj[KEY_CONFIDENCE])
            color = obj[KEY_COLOR]

            str_label = "{}: {:.1f}%".format(label, confidence * 100)
            # str_label = "{}".format(label)

            cv2.rectangle(show_img, (x + ofst_x, y + ofst_y), (x2 + ofst_x, y2 + ofst_y),
                          color, 2)
            cv2.putText(show_img, str_label, (x + ofst_x, y + ofst_y), cv2.FONT_HERSHEY_SIMPLEX, 0.5,
                        COLOR_TEXT, 2)

        return show_img

    @staticmethod
    def show_trackers(trk_img, trackers, mode="rect"):
        show_img = trk_img.copy()

        for tid in trackers.keys():
            color = trackers[tid][KEY_COLOR]
            lbl = trackers[tid][KEY_LABEL]
            score = trackers[tid][KEY_CONFIDENCE]

            str_lbl = "{}_{}({:.1f}%)".format(tid, lbl, score * 100)
            # str_lbl = "{}({:.1f}%)".format(lbl, score * 100)

            [t_x, t_y, t_w, t_h] = trackers[tid][KEY_RECT]
            t_x2 = t_x + t_w
            t_y2 = t_y + t_h

            if mode == "point":
                cv2.circle(show_img, (int(t_x + t_w / 2), int(t_y + t_h / 2)), 5, color, -1)
            else:  # "rect"
                cv2.rectangle(show_img, (int(t_x), int(t_y)), (int(t_x2), int(t_y2)), color, 2)
                cv2.putText(show_img, str_lbl, (int(t_x), int(t_y)), cv2.FONT_HERSHEY_SIMPLEX, 0.5, COLOR_TEXT, 2)

        return show_img
