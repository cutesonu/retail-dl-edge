

# [ROOT]
import os
_cur_dir = os.path.dirname(os.path.realpath(__file__))
ROOT_DIR = os.path.join(_cur_dir, os.pardir)
LOG_DIR = os.path.join(ROOT_DIR, 'logs')
MODEL_DIR = os.path.join(ROOT_DIR, 'models')

# [KEYS]
# keys of detect object
KEY_FRECT = "float_rect"    # [x, y, x1, y1], range(float) = [0.0, 1.0], [0.0, 1.0]
KEY_COLOR = "color"
KEY_LABEL = "label"
KEY_CONFIDENCE = "confidence"
KEY_RECT = "rect"           # [x, y, w, h], range(int) = [0, img_width], [0, img_height]
# Keys of tracker object
KEY_TID = "tracker_id"
KEY_TRACKER = "tracker"
KEY_PREV_RECT = "prev_rect"

# [TRACKERS]
TRK_DLIB = "DLIB"
TRK_CSRT = "CSRT"
TRK_MOSSE = "MOSSE"

# [DETECTORS]
DET_YOLO2 = "YOLO2"
DET_YOLO3 = "YOLO3"
DET_SSD = "SSD"
DET_VGG = "VGG"
DET_DARKNET3 = "DARKNET3"

# [tracking parameter]
KEY_UPDATE_STATE = "update_status"
KEY_DWELLING_FRAMES = "dwelling_frames"
KEY_ENTER_FRAME_POS = "entered_frame_pos"
UPDATE_STATE_THRESH = 10

OVERLAP_THRESH = 30
ORDER_PT_RANGE_THRESH = 30
GOOD_TRACK_QUALITY = 5

SKIP1 = 1
SKIP2 = 20

#
SCALE_FACTOR = 1.0
